import { memory } from "wasm-gol/wasm_gol_bg";
import { Universe } from "wasm-gol";

const CELL_SIZE = 8; // px
const GRID_COLOR = "#222";
const DEAD_COLOR = "#1b1b1b";
const ALIVE_COLOR = "#EEEEEE";

const universe = Universe.new();
const width = universe.width();
const height = universe.height();

const canvas = document.getElementById("gol-canvas");
canvas.height = (CELL_SIZE + 1) * height + 1;
canvas.width = (CELL_SIZE + 1) * width + 1;

const ctx = canvas.getContext('2d');

const drawGrid = () => {
  ctx.beginPath();
  ctx.strokeStyle = GRID_COLOR;

  // Vertical lines.
  for (let i = 0; i <= width; i++) {
    ctx.moveTo(i * (CELL_SIZE + 1) + 1, 0);
    ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * height + 1);
  }

  // Horizontal lines.
  for (let j = 0; j <= height; j++) {
    ctx.moveTo(0,                           j * (CELL_SIZE + 1) + 1);
    ctx.lineTo((CELL_SIZE + 1) * width + 1, j * (CELL_SIZE + 1) + 1);
  }

  ctx.stroke();
};

const getIndex = (row, column) => {
  return row * width + column;
};

const bitIsSet = (n, arr) => {
  const byte = Math.floor(n / 8);
  const mask = 1 << (n % 8);
  return (arr[byte] & mask) === mask;
};

const drawCells = () => {
  const cellsPtr = universe.cells();
  const cells = new Uint8Array(memory.buffer, cellsPtr, width * height / 8);

  ctx.beginPath();

  for (let row = 0; row < height; row++) {
    for (let col = 0; col < width; col++) {
      const idx = getIndex(row, col);

      ctx.fillStyle = bitIsSet(idx, cells)
        ? ALIVE_COLOR
        : DEAD_COLOR;

      ctx.fillRect(
        col * (CELL_SIZE + 1) + 1,
        row * (CELL_SIZE + 1) + 1,
        CELL_SIZE,
        CELL_SIZE
      );
    }
  }

  ctx.stroke();
};

canvas.addEventListener("click", event => {
  const boundingRect = canvas.getBoundingClientRect();

  const scaleX = canvas.width / boundingRect.width;
  const scaleY = canvas.height / boundingRect.height;

  const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
  const canvasTop = (event.clientY - boundingRect.top) * scaleY;

  const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
  const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

  universe.toggle_cell(row, col);

  drawGrid();
  drawCells();
});


let animationId = null;

const isPaused = () => {
  return animationId === null;
};

const playPauseButton = document.getElementById("play-pause");

const play = () => {
  playPauseButton.textContent = "pause";
  renderLoop();
};

const pause = () => {
  playPauseButton.textContent = "play";
  clearTimeout(animationId);
  animationId = null;
};

playPauseButton.addEventListener("click", _ => {
  if (isPaused()) {
    play();
  } else {
    pause();
  }
});

let tps = null;
const tpsSlider = document.getElementById("tps");

const handleTpsChange = _ => {
    tps = tpsSlider.value;
    console.log(tps);
};

tpsSlider.addEventListener("change", handleTpsChange);

handleTpsChange();

const reroll = document.getElementById("reroll");

reroll.addEventListener("click", () => {
    universe.reroll();
});

const renderLoop = () => {
  animationId = setTimeout(renderLoop, 1/tps * 1000);
  universe.tick();
    
  drawGrid();
  drawCells();    
};

play();
